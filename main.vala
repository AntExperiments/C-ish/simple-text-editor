///
/// Program-Name:   Simple Text Editors
/// Description:    Allows the user to write and save text-files
/// Author:         ConfusedAnt (Yanik Ammann)
/// Prerequisites:  none
/// Build-command:  valac --pkg gtk+-3.0 main.vala; ./main
/// Git-Repository: https://gitlab.com/ConfusedAnt/
/// Versions (version-number, date, author, description):
/// v0.1, 14.12.2019, ConfusedAnt, starting developement
///
/// License:        This program is free software: you can redistribute it and/or modify
///                 it under the terms of the GNU General Public License as published by
///                 the Free Software Foundation, either version 3 of the License, or
///                 (at your option) any later version.
///
///                 You should have received a copy of the GNU General Public License
///                 along with this program.  If not, see <http://www.gnu.org/licenses/>.
///

using Gtk;

class RandomBoyTask : Window {
  TextView text_view;
  
	RandomBoyTask () {
		// set header parameters
		var header = new HeaderBar ();
		header.title = "Application Title";
		header.show_close_button = true;
		// set window parameters
		window_position = WindowPosition.CENTER;
		set_titlebar(header);
		set_default_size (500, 300);
		border_width = 2;
		
		// define buttons
		Gtk.Button open_button = new Gtk.Button.from_icon_name ("document-open");
        open_button.clicked.connect (on_open_clicked);
        
		Gtk.Button save_button = new Gtk.Button.from_icon_name ("document-save");
        save_button.clicked.connect (on_save_clicked);
        
        // add buttons to header
        header.add(open_button);
        header.add(save_button);      
        
        // add textView
        text_view = new TextView ();
        text_view.wrap_mode = WORD;
        
        // create window that's scrollable and has textView in it
        var scroll = new ScrolledWindow (null, null);
        scroll.set_policy (PolicyType.AUTOMATIC, PolicyType.AUTOMATIC);
        scroll.add (text_view);
        var vbox = new Box (Orientation.VERTICAL, 0);
        vbox.pack_start (scroll, true, true, 0);
        add (vbox);
        
	}
	
	private void on_open_clicked () {
        var file_chooser = new FileChooserDialog ("Open File", this,
                                      FileChooserAction.OPEN,
                                      "_Cancel", ResponseType.CANCEL,
                                      "_Open", ResponseType.ACCEPT);
        if (file_chooser.run () == ResponseType.ACCEPT) {
            open_file (file_chooser.get_filename ());
        }
        file_chooser.destroy ();
    }
    private void open_file (string filename) {
        try {
            string text;
            FileUtils.get_contents (filename, out text);
            this.text_view.buffer.text = text;
        } catch (Error e) {
            stderr.printf ("Error: %s\n", e.message);
        }
    }
    
    private void on_save_clicked () {
        var file_chooser = new FileChooserDialog ("Save File", this,
                                      FileChooserAction.SAVE,
                                      "_Cancel", ResponseType.CANCEL,
                                      "_Save", ResponseType.ACCEPT);
        if (file_chooser.run () == ResponseType.ACCEPT) {
            save_file (file_chooser.get_filename ());
        }
        file_chooser.destroy ();
    }
    private void save_file (string filename) {
        try {
            string text;
            FileUtils.set_contents (filename, this.text_view.buffer.text);
        } catch (Error e) {
            stderr.printf ("Error: %s\n", e.message);
        }
    }
			
	static int main (string[] args) {
		init (ref args);

		// create window
		var window = new RandomBoyTask();
		window.destroy.connect(main_quit);
		window.show_all();

		Gtk.main ();
		return 0;
	}
}